Find the odd int

https://www.codewars.com/kata/54da5a58ea159efa38000836

- given an array of integers, find the one that appears an odd number of times

test_arr1 = [1,2,3,7,7,3,1,1,1,2,6,7,6] answer: 7
test_arr2 = [10,55,55,10,12,14,12,14,55] answer: 55
test_arr3 = [1,1,2,2,3] answer: 3
test_arr4 = [5,4,68,90,4,5,4,5,4,5,90,68,90] answer: 90
test_arr5 = [1] answer: 1
test_arr6 = [2,2,1] answer: 1
test_arr7 = [3,3,3,3,3,3,3] answer: 3
test_arr8 = [4,3,4,3,4,3,4,3,4,3,4,3,4] answer: 4
test_arr9 = [1,2,3,1,2,3,1,2,3,1,2,3,1] answer: 1
